
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseIdEntity } from '../../config/index';
export class FindCatsByIdDto extends BaseIdEntity {
  @ApiModelProperty({
    required: true,
  })
  id: string;
}
