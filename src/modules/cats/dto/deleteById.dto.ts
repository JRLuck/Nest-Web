
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseIdEntity } from '../../config/index';
export class DeleteById extends BaseIdEntity {
  @ApiModelProperty({
    required: true,
  })
  id: string;
}
