import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseIdEntity } from '../../config/index';
export class UpdateCats extends BaseIdEntity {
  @ApiModelProperty()
  @IsNotEmpty()
  readonly id: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly name: string;
  @ApiModelProperty()
  @IsNotEmpty()
  readonly age: number;
}
