import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  ForbiddenException,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { ApiResult, CurrentUser } from '../config/index';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
// import { JwtAuthGuard } from '../auth';
import { CatsService } from './cats.service';
import { FindCatsByIdDto } from './dto/FindCatsById.dto';
import { CreateCats } from './dto/createCats.dto';
import { UpdateCats } from './dto/updateCats.dto';
import { DeleteById } from './dto/deleteById.dto';

@Controller('cats')
@ApiUseTags('Cats管理')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}
  @Get('getList')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async findAll(): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.findAll());
  }

  @Post('findById')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async findById(@Body() payload: FindCatsByIdDto): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.findById(payload.id));
  }
  @Post('add')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async createCats(@Body() payload: CreateCats): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.add(payload));
  }
  @Post('update')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async updateCats(@Body() payload: UpdateCats): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.update(payload));
  }
  @Post('delete')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async deleteCats(@Body() payload: DeleteById): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.delete(payload));
  }
  @Post('findByAgeAndName')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async findByAgeAndName(@Body() payload: CreateCats): Promise<ApiResult> {
    return ApiResult.SUCCESS(await this.catsService.selectCats(payload));
  }

  @Post('error_Test')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  async errorTest(@Body() payload: CreateCats): Promise<ApiResult> {
    throw new HttpException(
      '',
      HttpStatus.FORBIDDEN,
    );
  }
}
