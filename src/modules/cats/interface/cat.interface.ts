export interface Cat {
    readonly id: string;
    readonly name: string;
    readonly age: number;
  }