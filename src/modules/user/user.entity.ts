import { Exclude } from 'class-transformer';
import { Entity, Column, PrimaryColumn } from 'typeorm';
import { PasswordTransformer } from './password.transformer';
import { BaseIdEntity } from '../config/index';

@Entity({
  name: 'users',
})
export class User extends BaseIdEntity {
  @PrimaryColumn()
  id: string;

  @Column({ length: 255 })
  firstName: string;

  @Column({ length: 255 })
  lastName: string;

  @Column({ length: 255 })
  email: string;

  @Column({
    name: 'password',
    length: 255,
    transformer: new PasswordTransformer(),
  })
  @Exclude()
  password: string;
}

export class UserFillableFields {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}
