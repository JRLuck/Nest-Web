import { User } from 'modules/user';
import { tap } from 'rxjs/operators';
import {
  ExecutionContext,
  NestInterceptor,
  CallHandler,
  Injectable,
} from '@nestjs/common';

@Injectable()
export class CurrentUser implements NestInterceptor {
  private user: User;
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): import('rxjs').Observable<any> | Promise<import('rxjs').Observable<any>> {
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();

    return next.handle().pipe(
      tap(
        () =>
          //   console.log(`用户${request.user ? request.user.email : '未登录'}`),
          (this.user = request.user),
      ),
    );
  }
  public  get  currentUser() {
    return this.user;
  }
}
