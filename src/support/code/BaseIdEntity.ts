import { v1 } from 'uuid';
export class BaseIdEntity {
  public id: string = v1();
}
